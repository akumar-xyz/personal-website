---
title : "Contact"
date : 2019-01-07T20:49:28+05:30
categories : 
- misc
draft : false
type : mainpage
menu :
    sitenav_l:
        name: "Contact"
        weight: 3
        pre : '<i class="far fa-envelope-open"></i>'
---

# Socials and contact details

I don't particularly  care much for social media, however if you need to get a
hold of me, you can find me lurking in the following places

<i class="far fa-envelope"></i> Email - [adi@akumar.xyz](MAILTO:adi@akumar.xyz)

<i class="fab fa-gitlab"></i> GitLab - [GitLab.com/SillyPill](http://www.gitlab.com/sillypill)

<i class="fab fa-linkedin"></i> LinkedIn - [linkedin.com/in/adithya-kumar-xyz](https://www.linkedin.com/in/adithya-kumar-xyz)

### Scan this QR code for a vCard

[![vCard](/qrcode.png)](/vCard.vcf)

# PGP

```txt
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQGNBF+yoYsBDAC6ewzVC/nQ5Tq1ZtjY8TPh6p0Nn5cANU2Mt7OC1aW61KbZgH5m
1sHXzYPkIoccHimZ2ha8bPCg4syAq/iWms5EMxP8BER98XS1FnXdkfW/44lvQQtS
bapYzSAs1/+ZRqGxuGLDZ8Z/Y+C9n4ng0F27vUq7ayUQw4Bea2kaROan26whMh8d
urgZMCKPO6Pl1xclVVyEH8T/yV9zwVORYWeEeFbca7uS1fa1+n7Bx3yIfJyXIPa5
jRS2R3fRnod5IbhGOvSr++vSdw1DNN9qehY5pzjas6eyb77HmwQcp98vDMIWJ3t1
czPQNhXHp1N62s/HF9h9FDI3Ev8do/Agj5pFmrwfxhXNa2mRug9Xc/ByQe4g/HlL
2kiUbulXH6rOZ96GbTpG2Zb+f1CFPSOTNq7CUqUxep2rVLUDC3T/jGkpBmT+XXWI
Ap/M6FAl/74Ef8rmpyjH2rXx01URr67LMXhqVWQEJHRwSgqR73lOOXYsuq8hW6Yy
rTCXJNNXMo1COi8AEQEAAbQXYWt1bWFyIDxhZGlAYWt1bWFyLnh5ej6JAc4EEwEI
ADgWIQR7q7kw2f3cpMTD1zbHICcDhVs7nQUCX7KhiwIbAwULCQgHAgYVCgkICwIE
FgIDAQIeAQIXgAAKCRDHICcDhVs7nSztDACGDyp24pck3UveY0JwEXdg6YPo/95K
/pSV0PWnlftNyKar29Zzkem196XJa8+8LTD5MYEyAZtAAw2+kL+TJjzYFO7/Upiy
d7p/8SnI0FgnpkdCLHRkpj0/8YizP3epJHdldDERuRHUigsNgaPgMfTnnQHxbn6m
tOaG7X1Lo7nCIHa6+t/iQValAjbDKZkpX8xQeStYgH/OOXQxZWofm4DXg6VZ/sDF
Dw5VLzmPjitBA8LYITHOJ+L3VgG3QJuvXU9GHX3iuZhpOXXgQ3SWzA96LlmqOxOW
XBNbZK2Itb/ARjIJzIdhcjP+OjmIg55XPDQ2mX0kZCBSu/8wjEOSu1gNLVB8So9m
LcubUbwd9HYUl4U8ZygrylrlQVID5kKc83uVau5WgR1yT32UiIKutHwoH62OvoaA
4Lc81t/VGEcR4/Q1QNlrfXuQz9mExqEj+xBxY7PxfBURDIKTRKJEbLsk0nWG68+C
IIekE1R/6fH6T4mZ425rf//dK9C1VLOErwW5AY0EX7KhiwEMALIpsB2ZF6OClLIk
G+99l0kL77ylIlD2BdHHWDMlpEp6924aIsUg4Xl4CD0JmjD61TKVk1RWrnxp/UDO
xc33tQW+UPrzIArfiu8rE846wvPOrv/WmprgHqoLpXKu4rFBzYUbdoVcA9vtLAef
Y0L3coLK+sqO3M/AF9LKtLX/AA3HI+pSJdc6UOO9ZXVfd1GhB+F2+CpptazLOqoK
+uUgfgBVvmTAr0C2eNRMMz9AAe1grk2+71N/2Y7IuAbdDZitZ4dzeYu3GrGIJ6iz
d7J/Lm9mQFKCFXTIH0INOHG4cifQkbE3BgiFh6fzuTbKNBvnVNoZZsj0IFE04QI0
U5E90vC4JEh1Ch3Ddr5kQK28NJzaSTaXg4TcbvmHE+XyMdcFSRnT021o7rJqH42+
Gyl0vJvlacXRXIQZ7pBUx8v0SHTi+KIp2Zv0jpNrNgUu8V4SsTDTrumZfWlTQV5j
P8h+/WYMwA0ZN14J39HOiP1NVxKozEDmlNA6FuAx0CCdUM0AmQARAQABiQG2BBgB
CAAgFiEEe6u5MNn93KTEw9c2xyAnA4VbO50FAl+yoYsCGwwACgkQxyAnA4VbO50W
7AwAnl2BBXgamBjNenUefBVEU/DMQ2qH+dkuQQGkM9tJVaeqlKapgL7rXXQyDFVB
vqenAz/EDgyRnAY8loX2kmwQkmkHJ7a2rzr6483MxkkCAaR57hCgOP8+iX52WBBR
+p02Z7Aq1g3caEM6iNRYQte84gmbiI0OlKp+AIDmU10YhcCSBj/9deZW1LLakiwx
uM4551+nKxdAueWc6Nk+Tu6sGIf+qj75TJOZTC8tV1aG1ktL3YOgHZL3s0wj0jqx
le2bvQi1wkghTaOIiSb9hIV/7QE0GsQrufbDL/BCfSo99Y0IFXuBOT8VF7o2VZ16
i4YiY9yoa83r2n1JkIG97RxrrbM9h9aWuo9Cld22YR8aC2/QTttGuzz5Q9chHVpV
6+cXFYN3H04UAulVYAs0uBPm2AFl8uIGpibmcpJBVLjU8lGZ1S/9wyXsNuEqYwta
GfKXk1b5vgXIAj040LRwZHtpXgT0DYS6FOp/AY6Cv0yANHIeQ+uDW7P9vonucYcx
sOGm
=lWgm
-----END PGP PUBLIC KEY BLOCK-----
```
