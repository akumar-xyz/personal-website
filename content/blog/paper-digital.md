---
title: "Digital Media vs Printed Media"
date: 2021-12-11T14:13:34+05:30
draft: false
categories:
- Misc
Tags:
- rant
---

![Books journals and laptop](/img/blog/papervsdigital.jpg)

## Printed Media

Ugh paper websites...

- Missing features: [**no hyperlinks**](#), interactive content, media playback
- they're heavy; digital media do not have weight
- they're made of carbon
- they're expensive to print
- they can't be updated over time

Some printed books could REALLY use hyperlinks. Especially when the information
presented is not linear in nature (textbooks, guides, journals). I have seen
many books where internal references are done without even mentioning page
number (WTH!).

I've been asking for the hyperlink feature forever but I don't think its coming
anytime soon. 😞

## Digital Media

Ugh internet documents...

- they can't be used without 21st century infrastructure.
- flipping through and browsing pages is slow and tedious. On the other hand, I can
  flip through literally hundreds of paper pages in a second.
- Missing features: scribbling and tearing. Tearing paper and scribbling on it
  is possible. It is very easy. Sometimes this feature is super useful.
- difficult to read for long durations of time; human eyes get tired and irritated when
  looking directly into a light source for long-ish times. Unfortunately,
  that's the only way to consume digital media these days. Printed books do not
  have this problem.

I've been waiting forever for affordable, usable reflextive displays but I
don't think I can buy one anytime soon. 😞

## On E-readers

Have their purpose and are very good for certain use cases. But also, inherits
disadvantages from both other mediums.

## In conclusion

As a written media consumer, paper books with hyperlinks for references would
be awesome. Until that's possible, if all our textbooks were websites
*and* we had reflective displays on our devices, that can be acceptable too.
Somebody make it happen. Thank you very much.

