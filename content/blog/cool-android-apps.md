---
title: "Cool Android Apps (on F-droid)"
date: 2021-10-01T00:00:00+05:30
draft: false
categories:
- Misc
Tags:
- misc
---

### 1. [Notification Cron ](https://f-droid.org/packages/com.github.fi3te.notificationcron)

Uses cron expressions to schedule regular notifications. Functions as
advertised. I have set up reminders for all sorts of things; reminders to drink
water, wash my hands, check my mail, and to use dim lights etc. I'm sure you
could think of a couple of additional uses as well.

I'd recommend using the app along with a watch that can display notifications
unless you're trying to live inside your smartphone all day.

### 2. [Croc](https://f-droid.org/packages/com.github.howeyc.crocgui)

Sends files from one *device* to another device- over the internet. The best
part is- it just werks. Very nice.

### 3. [Open in Whatsapp](https://f-droid.org/packages/io.github.subhamtyagi.openinwhatsapp)

Ever want to send a quick messages to somebody on whatsapp without actually
saving their contact? 'Open in Whatsapp' uses Whatsapp's public chat API to
open a chat with any phone number. Really useful for sending location to
somebody not in your contact list or creating groups.

### 4. [DNS66](https://f-droid.org/packages/org.jak_linux.dns66)

Creates a "VPN service" on your phone to block advertisements system-wide.
Works great!  Easy to set up and no rooting shennanigans required. Only
drawback is that we can not chain VPNs on android. So incase you are already
using a VPN service, for instance to bypass geo-blocking, this approach to
adblock will not work.


## Quick word about living with F-droid and without the playstore (going ungoogled)

[F-droid](https://www.f-droid.org) + [IggyOnDroid](https://gitlab.com/IzzyOnDroid/repo) covers 90% off all them "app" things I need on my phone.
MicroG is great. The AOSP apps serve their purpose adequately. However, for the
remaining 10% (Whatsapp, banking, Google Maps) I rely on the Aurora Store
which is surprisingly robust and functional.

It is difficult to find simple apps without ads on the playstore. I also
struggle to find apps that I am confident are not doing evil things with my
data. On the other hand, in my experience, the quality of software on F-droid
is generally better. 

I hope this list has been useful to somebody and has encouraged you to check
out Fdroid.

