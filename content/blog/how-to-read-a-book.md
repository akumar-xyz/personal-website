---
title: "Book 👏 Review 👏 : How to Read a Book"
date: 2019-07-06T22:25:51+05:30
draft: false
categories:
- Misc
Tags:
- productivity
- book
- review
---

![How to Read a Book Cover](/img/how-to-read-a-book/how-to-read-a-book-cover.jpg)


# About the Book

The title of the book is not artsy-fartsy. 'How to Read a Book' perfectly
describes what the book aims to achieve. It intends to introduce the reader to
some very simple but useful \*LIFE HACKS\* for reading books. Then, the book does
just that, competently.

**From the first paragraph of the first chapter in the book :**

> This is a book for readers and for those who wish to become readers.
> Particularly, it is for readers of books. Even more particularly, it is for
> those whose main purpose in reading books is to gain increased understanding.

There are two editions of 'How to Read a Book'. The first edition was published
in 1940, and then the **significantly** revised second edition was published in
1972. It has to be mentioned, the differences between the two editions really
are *quite* significant. I've read the revised 1972 edition.

# How to read "How to Read a Book"

Assuming that you don't know how to read a book yet, allow me to explain the
proper way to read a book according to "How to Read a Book"

## Level 1 : Skimming

1. Look at the title page and the 'preface'
2. Study its 'Table of Contents'
3. Glance at the 'index' at the end of the book
4. Read the publisher's blurb
    - If you're reading from a physical book, the blurb is usually on the back
      cover of the book
    - In case of e-books and such, the blurb is usually [the description on the product page](https://www.amazon.com/How-Read-Book-Classic-Intelligent/dp/0671212095)
5. Look at the chapters that seem the most important to you
6. Flip through the book reading a paragraph or two, even several pages in
   sequence but never more than that.

## Level 2 : Inspectional Reading

1. Read through the book. It doesn't matter how deeply you grasp all the
   concepts conveyed in this book at this stage. What's important is that you
   read everything once.
2. Make notes, comments, references, mindmaps etc. along the way.

**Note : This much is adequate for most books. Proceed to step 3 \*IF\* you believe that
it is necessary**

## Level 3 : Analytical Reading

From the book:

> Analytical reading is thorough reading, complete reading, or good reading-the
> best reading you can do. If inspectional reading is the best and most
> complete reading that is possible given a limited time, then analytical
> reading is the best and most complete reading that is possible given
> unlimited time.

The authors go into detailed descriptions of analytical reading. For more
details, see Level 2.

# Final Thoughts and Conclusions

There are many things to appreciate in this book. The second half primes the
readers with useful ways to approach to different types of books such as
'Literature', 'Philosophy' or 'History', in order to  make you a better and
more demanding reader. And I find it very interesting, that a skills as
fundamental and useful as these, not not a part of the academic curriculum in
schools.

[Contrary to popular beliefs, people read more than ever
before](https://www.forbes.com/sites/neilhowe/2017/01/16/millennials-a-generation-of-page-turners/).
A good chunk of that reading is internet comments, text messages, blogs and
articles etc. Given all that, in my experience, "How to Read a Book" is still
very relevant. A lot of what is in the book applies to other forms of written
media also. In essence, you can take away a lot from this book even if you're
not necessarily an avid reader. But the book might just change your mind.

In my opinion, the best way to approach "How to Read a Book" is as you would a
text book, or even a reference book. However, approaching the book as a purely
academic exercise rather than a practical guide is a completely wasteful.

# tl;dr 
**Should you read : How to Read a Book ?** 

Up to you. :)

...I found it very useful and informative.

