---
title: "My Experience With Smart India Hackathon"
date: 2019-09-16T21:24:40+05:30
draft: false
categories:
- Misc
Tags:
- misc
---

### Some background about myself

I have participated in all the three Smart India Hackathons, the
software editions conducted so far (2017, 2018 and 2019).

In 2017, our team competed for the Department of Posts problem statements at
[JECRC, Jaipur](https://www.jecrcfoundation.com/).  Without getting into too
much detail, we worked on a barcoding system to track/sort posts and parcels.
We sucessfully bagged the 1st prize.

In 2018, my team competed for the Ministry of Railways problem statements at
[Techno NJR, Udaipur](https://technonjr.org). We worked on a public address
system that could reach personal devices. We were able to win the 1st prize.

In 2019, our team participated for the Ministry of Civil Aviation problem
statement at [Sathyabama Institute of Science and Technology,
Chennai](http://www.sathyabama.ac.in/) and were again able to win in our
problem statement category. 

So I figured, that with the recent announcement of SIH 2020, I am as qualified
as anyone else might be to share some insights that have helped me out in the
past.

## Problem Statements and Solutions

- The format of SIH may change drastically over the years. If you're given a
  list of problem statements to choose from, it is ideal to spend a lot of time
  (atleast a couple of days) with them. I find it inconvenient to study the
  problem statements using the default web interface. Consider scraping all
  that problem statements from the website and putting them in a `.csv` or a
  spreadsheet file. A file like this can also significantly assist discussions
  within a team especially if you put it up on Google Docs.

- **Good solutions are simple solutions.** Well... not all the time, but its a
  good rule of thumb to rely on. If you cannot describe your solution in a
  short 30 second [elevator pitch](https://www.bing.com/search?q=elevator+pitch),
  it usually means that there is room to refine your idea.

> *"The notion of 'intricate and beautiful complexities' is almost an
> oxymoron."* -Doug McIlroy 

- **Design your solution for the future, not *just* the present.** Because the
  future will be here sooner than you think -- another important piece of
  advice I've borrowed from [The Art of UNIX
  Philosophy](https://homepage.cs.uri.edu/~thenry/resources/unix_art/).

- *The ideation stage of problem solving is very important.* So don't skimp on
  effort here. If you have any queries regarding the problem statements, do not
  hesitate to write to an e-mail if it is provided. Otherwise find a person you
  can ask questions to.  Regardless of whether or not your solution makes it to
  the final stage of the actual hackathon event, this is where a large section of  learning really happens.

- The Smart India Hackathon is different from most other hackathons in that you
  are attempting to solve real life problem statements given to you.
  Practicality and feasibility have higher priority here. If the solution to a
  problem basically amounts to throwing money at it, for example, that won't do.

## The 36-Hour Hackathon Event

If you were able to get through to the finale, congratulations! In 2019, only
1 out of 30 or so idea submissions made it through. At this point, you can be
fairly confident that your idea is worth something.

- It is very likely that you will be travelling across the country for the
  finale hackathon event. **If possible, reach one day early** and get
  acclimatized to the weather. Get to know the organizers, volunteers and the
  city. Try to relax while you have the time. Staying up for more that 24 hours
  is taxing even for the more seasoned hackathon participants.

- Remember, a hackathon is NOT *just* a "coding competition". The idea is to
  spend the given time developing a product. So selling the product is equally
  as important. Not to say that your coding skills will not be put to the test,
  however, it is a good idea to have one or two team members working exclusively
  on presentations for the judging rounds.

- The rules regarding **half-baked code** was mentioned only in the [SIH2017
  FAQs](https://innovate.mygov.in/faq-sih/), but never after that (if I'm not
  wrong). It is generally a good idea imho. However, it is important to keep in
  mind that your solution is absolutely bound to change during the hackathon
  event. It would be ill-adviced to rely on half-baked code completely.

- **Take good advantage of your time with the judges and mentors.** You will
  interact with a lot of accomplished industry veterans during that course of
  36-hours, engineers or otherwise. They have likely taken valuable time out of
  their weekends to engage with participants for this opportunity. Pay
  attention to their insights, about the product or otherwise.  Some of the
  judges/mentors might not have an engineering background, it is useful to be
  mindful of that.

## Misc. Hackathon tips & tricks

- Nobody is going to check your code (probably).  So **half-assed code is
  acceptable, as long as its the correct half of the ass.** Time is precious,
  spend it on developing a "proof of concept".
  For example, if a login interface is not core to your solution, but simply
  essential, a placeholder login page with client side authentication is usually
  acceptable.
  {{< highlight js >}}
if ( username == "admin" && password == "admin" ) {
    doLogin();
} else {
    showError("Invalid credentials");
} {{< / highlight >}}
  Just don't be completely obnoxious with your jugaad. An else-if ladder is not
  a "decision tree algorithm using machine learning".

- **NO. SOFTWARE. UPDATES.** It's tempting. I know. Especially if you're given
  a good network connection. But don't. Not only does it use up precious
  bandwidth which you share with fellow participants, but if the update breaks
  something *during* the hackathon, you will have one team member down and the
  whole experience will traumatize you emotionally. Unfortunately, Windows 10
  users may not get a choice in this regard.

- If a hackathon event goes on for 36 hours, you will get only 24 hours to work
  on your product. If a hackathon event goes on for 24 hours, you will get only 16.
  It might be the fatigue, or maybe the way hackathons are organized, or
  even the fact that the final round of judgement generally feels like a mere
  formality, or a combination of all these factors and more. Plan accordingly.

- **It pays to be prepared.** It's the little things that make a big
  difference. The organizers are usually able to oblige to all your
  requirements. However, in case they aren't able to help, simple things like
  bringing a spare extension box, or a router can help you out in a big way.
  Put all code into a version control. You may not always need it, but it pays
  to be prepared.

## Final thoughts

**My experience with SIH is not going to be your experience of SIH**. So I
can't tell you much about what you should expect. So whatever you encounter,
have fun with it. Be open to opportunities. Hackathons are an excellent place
to network and meet new people/ideas.

I, for one am very greateful to all the organizers (i4c, AICTE, nodal centers)
and prime minister Shri Narendra Modi for all the opportunities and
experiences.

Like. Share. Subscribe. Thanks.
