---
title: "How I Learned to Stop Worrying and Love the RSS"
date: 2019-06-07T00:31:20+05:30
draft: false
categories:
- Misc
Tags:
- productivity
- technology
- rss
---

***Note: This post is not about politics!***

![rss logo](/img/rss/rss.png)

You might have seen this orange button lying around the internet, here and
there, never giving it a second thought. In this post I explore the joys of the
orange buttons.


The orange button represents what is called an **RSS** ( **R**eally **S**imple
**S**yndication ) or an **Atom** feed, which is a very useful [web
standard](https://en.wikipedia.org/wiki/Web_standards) to keep track of
websites, podcasts, aggregators etc.

This post will also serve as a guide to effectively incorporate RSS into your
life to be more productive and informed. Basically, **this simple trick will BLOW YOUR
MIND!**

# So, what is an 'RSS'?

The best way to understand RSS is to think of it as an orange 'Subscribe' button.
A simple button to keep track of almost anything on the internet.
Here is a few things we can 'subscribe' to using RSS :

- News Sites
- Blogs
- Podcasts
- Google Groups
- YouTube channels... you name it

# The why?

Why care about something like RSS? Every app we use already gives us more
notifications than we asked for? Here are a few reasons why I care...

- I can use just one application for all my feeds.
- An RSS gives me a dumb feed. So there is no bell icon -- shadow
  ban -- broken notification shenanigans to worry about.
- One of the only ways I know to keep track of what I have and haven't 'read' yet

...etc

# Okay, here is what we need to do...

## Step 1 : Get an RSS Client

An RSS client is a software that manages all the feeds you are subscribed to,
and sends you notifications. The RSS clients come in different colors, shapes and 
sizes. These are the commonly recommended :

- TinyTinyRSS (Web based, you can use it on Chrome and Firefox)
- RSSOwl (Desktop Clients)
- Newsboat (Console based)
- Handy News Reader (Android)

## Step 2 : Give your RSS client some feeds to keep an eye on.

Uff... what is there to explain here?

**Rookie mistake #1** - Don't add feeds that you are not going to be interested
in.  Especially news feeds. News feeds are generally spam. Spam is literally
Hitler!

**Rookie mistake #2** - Trying to keep up and read all the unread feeds when
you don't have the time. If something that doesn't profit you feels like a
chore, you probably shouldn't be doing it.

Feel free to mark any post 'read', that you don't plan on reading.

#### Some RSS feeds you might be interested in : 

```sh
# A very important, useful and interesting blog
http://akumar.xyz/blog/index.xml

# Doordarshan News 
http://ddnews.gov.in/rss-feeds

# This website is aimed at UPSC aspirants. However, its a good 
# news source to keep track of current affairs regardless.
https://www.insightsonindia.com/feed/

# Infowars - Mainly US related things
https://rss.infowars.com

# no? okay try CNN Top Stories
http://rss.cnn.com/rss/edition.rss

```

### 2-A. Subscribing to a YouTube channel

The RSS feed link for any YouTube channel looks like this :

```sh
https://www.youtube.com/feeds/videos.xml?channel_id=<channel-id>
```

Just replace `<channel-id>` with an actual [channel
ID](https://www.youtube.com/watch?v=D12v4rTtiYM). Cool. Bob's your uncle.

#### Examples

```sh
# Subscribe to PewDiePie!
https://www.youtube.com/feeds/videos.xml?channel_id=UC-lHJZR3Gqxm24_Vd_AJ5Yw

# No? Okay.. Subscribe to T-Series!
https://www.youtube.com/feeds/videos.xml?channel_id=UCq-Fj5jknLsUf-MWSy4_brA

```

### 2-B. Subscribing to a Podcast

RSS feeds are very common in the podcasting scene. Perhaps the best way to find the feed for a podcast is :

[Podbay.fm](https://podbay.fm) -\> Search Podcast -\> Orange button (or ctrl+f 'RSS')

### 2-C. Subscribing to Google Groups
Yes. Orange button to be found at the 'About' page of a group.

## Step 3: ????
## Step 4: Profit!

# Also, Aaron Swartz

Finally, I believe that a discussion of RSS such as this can not be is complete
without the mention of Aaron Swartz, the internet's own boy. The following
video is an interview of Cory Doctorow, a friend of Aaron Swartz and an
esteemed author.

{{< youtube ZEEgb1cAwvk >}}
