---
title : "Home"
date : 2019-01-07T20:49:28+05:30
categories : 
- misc
draft : false
menu :
    sitenav_l:
        name: 'Home'
        weight: 1
        pre: "<i class='fas fa-home'></i>"
---

# Hello!

![welcome](/img/welcome.gif)

My name is Adithya Kumar. Welcome to my internet web site.


## I am… 
- a good with computers
- a data engineer at [KPMG Assurance and Consulting Services LLP](https://home.kpmg/in/en/home/about.html), Bengaluru
- currently reading: [One Hundred Years of Solitude.](https://www.goodreads.com/book/show/320.One_Hundred_Years_of_Solitude)

**Connect with me on [LinkedIn!](https://www.linkedin.com/in/adithya-kumar-xyz) I am (almost) active there.**


### Hope you find what you are looking for!

